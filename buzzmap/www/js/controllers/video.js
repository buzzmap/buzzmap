'use strict';
angular.module('AppCtrl',
        [
            "ngSanitize",
            "com.2fdevs.videogular",
            "com.2fdevs.videogular.plugins.controls",
            "com.2fdevs.videogular.plugins.overlayplay",
            "com.2fdevs.videogular.plugins.poster",
            "com.2fdevs.videogular.plugins.buffering"
        ]
    )

    app.controller('VidCtrl', ["$scope", "$sce", function($scope, $sce) {
        var controller = this;
        console.log(controller);
        controller.API = null;
        controller.onPlayerReady = function(API) {
            controller.API = API;
        };
        ionic.Platform.ready(function() {
            console.log("hello from video.js");
            var ref = new Firebase('https://buzzmapv0.firebaseio.com/');
            ref.on('value', function(snapshot) {
                $scope.message = snapshot.val();
                $scope.video_url = $scope.message.videos[0].url;
                console.log("video_url = " + $scope.video_url);
            });
            
            controller.videos = [
                {
                    sources: [
                        {src: $sce.trustAsResourceUrl("http://www.videogular.com/assets/videos/videogular.mp4"), type: "video/mp4"},
                        {src: $sce.trustAsResourceUrl("http://www.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
                        {src: $sce.trustAsResourceUrl("http://www.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
                    ]
                },
                {
                    sources: [
                        {src: $sce.trustAsResourceUrl("http://www.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov"), type: "video/mp4"},
                        {src: $sce.trustAsResourceUrl("http://www.videogular.com/assets/videos/big_buck_bunny_720p_stereo.ogg"), type: "video/ogg"}
                    ]
                }
            ];

            controller.config = {
                preload: "none",
                autoHide: false,
                autoHideTime: 3000,
                autoPlay: false,
                sources: controller.videos[0].sources,
                theme: {
                    url: "http://www.videogular.com/styles/themes/default/videogular.css"
                },
                plugins: {
                    poster: "http://www.videogular.com/assets/images/videogular.png"
                }
            };

            controller.setVideo = function(index) {
                controller.API.stop();
                controller.config.sources = controller.videos[index].sources;
            };

        
    });
}]);

    

  //var ref = new Firebase('https://buzzmapv0.firebaseio.com/');
    //var sync = $firebaseAuth(ref);
    //$scope.data = sync.$asObject();
  //$scope.ref = ref;

  
    

  

/**
.controller('CameraCtrl', [$scope, $cordovaCapture, function($scope, $cordovaCapture) {
   ionic.Platform.ready(function() {
    console.log('hello from video.js');
	$scope.videoCam = function() {
        var options = {
        					limit : 2,
        					duration : 60
        					};
        
        function captureSuccess(mediaFiles) {
           
        }
        Capture.prototype.captureVideo = function(successCallback, errorCallback, options) {
            _capture("captureVideo", successCallback, errorCallback, options);
        };
        module.exports = new Capture();

        /**
	    $cordovaCapture.captureVideo(captureOptions).then(function(videoData) {
            mediaFile.getFormatData(
            MediaFileDataSuccessCB successCallback,
            [MediaFileDataErrorCB errorCallback]
            );
            var i, len;
            for (i = 0, len = mediaFiles.length; i < len; i += 1) {
                uploadFile(mediaFiles[i]);
            }
	    },
        function(err) {
            var msg = 'An error occurred during capture: ' + err.code;
            navigator.notification.alert(msg, null, 'Whoops, error!');
        });

    };
    $scope.videos = "videos";
    $scope.take = function() {
        navigator.getUserMedia({video: true}, success, fallback);
    }
   }); 
}]);
**/