'use strict';
angular.module('AppCtrl', ['ngMap'])

app.controller('MapGeolocationCtrl', function($scope, $timeout) {
  ionic.Platform.ready(function() {
    $scope.$on('mapInitialized', function(event, map) {
      console.log("hello from map.js");
      if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        $scope.pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);
        var infowindow = new google.maps.InfoWindow({
          map: map,
          position: $scope.pos,
          content: 'Your location'
        });
        map.setCenter($scope.pos);
        //$scope.height = window.innerHeight + "px";
      },
      function() {
        handleNoGeolocation(true);
      });

      $scope.touchDisplay = console.log(false + "touchDisplay");

      $scope.home = function() {
        map.setCenter($scope.pos);
      }
      
      $scope.toggleSearch = false;
      if($scope.toggleSearch === false) console.log($scope.toggleSearch);

      var markers = [];
      $scope.dynMarkers = [];
        for (var i = 0; i < 8; i++) {
          $timeout(function() {
          markers[i] = new google.maps.Marker({ title: "Marker: " + i });
            var lat = 37.7749 + (Math.random() / 100);
            var lng = -122.4194 + (Math.random() / 100);
            var loc = new google.maps.LatLng(lat, lng);
            markers[i].setPosition(loc);
            markers[i].setAnimation(google.maps.Animation.Drop);
            markers[i].setMap(map);
            console.log(lat + lng);
          }, i * 1500);
          };
          $scope.markerClusterer = new MarkerClusterer(map, $scope.dynMarkers, {});
      } else {
        $scope.pos = new google.maps.LatLng(37.7749, -122.4194);
        handleNoGeolocation(false);
        alert(Location_err);
      }
      onGesture(release, $scope.showNavTools, 'map');

      var setMapHeight = function() {
        //A hack to set map at device screen height
        map.setAttribute( "style", "height:" + window.innerHeight + "px;" );
      }
      
      
    });
  });
});

app.controller('markerCtrl', ['$scope', function($scope) {
  function GeolocationMarker(opt_map, opt_markerOpts, opt_circleOpts) {

  var markerOpts = {
    'clickable': true,
    'cursor': 'pointer',
    'draggable': false,
    'flat': false,
    'icon': {
        'url': 'https://google-maps-utility-library-v3.googlecode.com/svn/trunk/geolocationmarker/images/gpsloc.png',
        'size': new google.maps.Size(34, 34),
        'scaledSize': new google.maps.Size(17, 17),
        'origin': new google.maps.Point(0, 0),
        'anchor': new google.maps.Point(8, 8)
    },
    // This marker may move frequently - don't force canvas tile redraw
    'optimized': false, 
    'position': new google.maps.LatLng(0, 0),
    'title': 'Current location',
    'zIndex': 2
  };

  if(opt_markerOpts) {
    markerOpts = this.copyOptions_(markerOpts, opt_markerOpts);
  }

  var circleOpts = {
    'clickable': false,
    'radius': 0,
    'strokeColor': '1bb6ff',
    'strokeOpacity': .4,
    'fillColor': '61a0bf',
    'fillOpacity': .4,
    'strokeWeight': 1,
    'zIndex': 1
  };

  if(opt_circleOpts) {
    circleOpts = this.copyOptions_(circleOpts, opt_circleOpts);
  }

  this.marker_ = new google.maps.Marker(markerOpts);
  this.circle_ = new google.maps.Circle(circleOpts);

  /**
   * @expose
   * @type {number?}
   */
  this.accuracy = null;

  /**
   * @expose
   * @type {google.maps.LatLng?}
   */
  this.position = null;

  /**
   * @expose
   * @type {google.maps.Map?}
   */
  this.map = null;
  
  this.set('minimum_accuracy', null);
  
  this.set('position_options', /** GeolocationPositionOptions */
      ({enableHighAccuracy: true, maximumAge: 1000}));

  this.circle_.bindTo('map', this.marker_);

  if(opt_map) {
    this.setMap(opt_map);
  }
}

  
}]);